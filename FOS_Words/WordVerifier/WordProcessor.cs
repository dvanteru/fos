﻿using System.Collections.Generic;
using System.Linq;
using FOS_Words.Segment.Contract;
using FOS_Words.WordVerifier.Contract;

namespace FOS_Words.WordVerifier 
{
    public class WordProcessor : IWordProcessor
    {
        private readonly IWordToListBasedOnLength wordToListBasedOnLength;

        public WordProcessor(IWordToListBasedOnLength wordToListBasedOnLength)
        {
            this.wordToListBasedOnLength = wordToListBasedOnLength;
        }

        public IEnumerable<string> WordsFormedOfTwoSubstrings(string text)
        {
            var completeWords = this.wordToListBasedOnLength.ExtractSixLetterWords(text);
            var substringWords = this.wordToListBasedOnLength.ExtractLessThanSixLetterWords(text);
            var cleanList = completeWords.Select(completeWord => CheckThisWord(completeWord, substringWords));
            return cleanList.Where(x => x != string.Empty);
        }

	/// <summary>
	/// This method is doing a bit too much. This can be further refactored. Probably a different algorithm.
	/// Due to time I didn't calculate the processing time, it takes if there are a million words in list.
	/// At the moment, as I am using loops time increases exponentially this can be further refactored to 
	/// increase in logarithmic as the list grows.
	/// </summary>
	/// <param name="completeWord">6 Letter word.</param>
	/// <param name="substringWords">Substrings used to form words.</param>
	/// <returns>6 letter word if condition is satisfied else returns empty string. </returns>
        protected string CheckThisWord(string completeWord, IList<string> substringWords)
        {
            var word = completeWord;
            foreach (var substringWord in substringWords.Where(substringWord => word.IndexOf(substringWord) >= 0))
            {
                word = word.Replace(substringWord, string.Empty);
                var secondPart = substringWords.Where(x => x == word).ToList().FirstOrDefault();
                if (secondPart + substringWord == completeWord || substringWord + secondPart == completeWord)
                    return completeWord;
            }
            return string.Empty;
        }
    }
}
