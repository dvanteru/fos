﻿using System.Collections.Generic;

namespace FOS_Words.WordVerifier.Contract
{
    public interface IWordProcessor
    {
        IEnumerable<string> WordsFormedOfTwoSubstrings(string text);
    }
}