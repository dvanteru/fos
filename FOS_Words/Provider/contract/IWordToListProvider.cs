﻿using System.Collections.Generic;

namespace FOS_Words.Provider.contract
{
    public interface IWordToListProvider
    {
        IList<string> ConvertToList(string text);
    }
}