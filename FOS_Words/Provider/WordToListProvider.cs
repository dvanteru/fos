﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using FOS_Words.Provider.contract;

namespace FOS_Words.Provider
{
    public class WordToListProvider : IWordToListProvider
    {
	/// <summary>
	/// This method is used to split provided text to List. I have managed to split the string with any non-alphabet character.
	/// </summary>
	/// <param name="text">This is the non-alphabetic delimeted string.</param>
	/// <returns>The list of words.</returns>
        public IList<string> ConvertToList(string text)
        {
            var regex = new Regex("[^a-zA-Z -]");
            text = regex.Replace(text, " ");
            return text.Split( new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries).ToList();
        }
    }
}