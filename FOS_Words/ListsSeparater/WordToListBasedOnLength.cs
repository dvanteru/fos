﻿using System.Collections.Generic;
using System.Linq;
using FOS_Words.Provider.contract;
using FOS_Words.Segment.Contract;

namespace FOS_Words.Segment
{
    public class WordToListBasedOnLength : IWordToListBasedOnLength
    {
        private readonly IWordToListProvider wordToListProvider;

        public WordToListBasedOnLength(IWordToListProvider wordToListProvider)
        {
            this.wordToListProvider = wordToListProvider;
        }

        public IList<string> ExtractSixLetterWords(string input)
        {
            var inputList = this.wordToListProvider.ConvertToList(input);
            return inputList.Where(x => x.Length == 6).ToList();
        }

        public IList<string> ExtractLessThanSixLetterWords(string input)
        {
            var inputList = this.wordToListProvider.ConvertToList(input);
            return inputList.Where(x => x.Length < 6).ToList();
        }
    }
}