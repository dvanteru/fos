﻿using System.Collections.Generic;

namespace FOS_Words.Segment.Contract
{
    public interface IWordToListBasedOnLength
    {
        IList<string> ExtractSixLetterWords(string input);
        IList<string> ExtractLessThanSixLetterWords(string input);
    }
}