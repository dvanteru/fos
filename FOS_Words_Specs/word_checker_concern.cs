﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FOS_Words;
using FOS_Words.Segment.Contract;
using FOS_Words.WordVerifier;
using FOS_Words.WordVerifier.Contract;
using Machine.Specifications;
using developwithpassion.specifications.extensions;
using developwithpassion.specifications.rhinomocks;

namespace FOS_Words_Specs 
{
    public abstract class word_checker_concern : Observes<IWordProcessor, WordProcessor>
    {
    }

    public class when_asked_to_check_list_words : word_checker_concern
    {
        private Establish c = () =>
            {
                text = "al, albums, bums, con, convex, con, eat, treaty";
                wordToListBasedOnLength = depends.on<IWordToListBasedOnLength>();
                sixLetterWords = new List<string>{ "albums", "convex", "treaty" };
                wordToListBasedOnLength.setup(x => x.ExtractSixLetterWords(text)).Return(sixLetterWords);
                lessThanSixLetterWords = new List<string>{ "al", "bums", "con", "eat", "vex" };
                wordToListBasedOnLength.setup(x => x.ExtractLessThanSixLetterWords(text)).Return(lessThanSixLetterWords);
            };

        private Because b = () => result = sut.WordsFormedOfTwoSubstrings(text);

        private It should_ask_word_converter_to_return_six_letter_words =
            () => wordToListBasedOnLength.received(x => x.ExtractSixLetterWords(text));

        private It should_ask_word_converter_to_return_less_then_six_letter_words =
                () => wordToListBasedOnLength.received(x => x.ExtractLessThanSixLetterWords(text));

        private It should_check_if_given_word_is_formed_of_two_existing_substrings = () => result.Count().ShouldEqual(2);

        private It should_return_the_word_albums = () => result.First().ShouldEqual("albums");

        private It should_return_the_word_convex = () => result.Skip(1).First().ShouldEqual("convex");

        private static string text;
        private static IEnumerable<string> result;
        private static IWordToListBasedOnLength wordToListBasedOnLength;
        private static List<string> sixLetterWords;
        private static List<string> lessThanSixLetterWords;
    }
}
