using System.Collections.Generic;
using FOS_Words.Segment.Contract;
using FOS_Words.WordVerifier;

namespace FOS_Words_Specs.TestingProtectedMethods
{
    public class WordProcessorTest : WordProcessor
    {
        public WordProcessorTest(IWordToListBasedOnLength wordToListBasedOnLength) : base(wordToListBasedOnLength)
        {
        }

        public new string CheckThisWord(string completeWord, IList<string> substringWords)
        {
            return base.CheckThisWord(completeWord, substringWords);
        }
    }
}