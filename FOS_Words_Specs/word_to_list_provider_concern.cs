﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FOS_Words;
using FOS_Words.Provider;
using FOS_Words.Provider.contract;
using Machine.Specifications;
using developwithpassion.specifications.rhinomocks;

namespace FOS_Words_Specs 
{
    public abstract class word_to_list_provider_concern : Observes<IWordToListProvider, WordToListProvider>
    {
        protected Establish c = () =>
        {
            text = constants.TEXT_INPUT;
        };

        protected static string text;
        protected static IList<string> returnValue;
    }

    public class When_input_with_text_is_provided : word_to_list_provider_concern
    {
        private Because b = () => returnValue = sut.ConvertToList(text);

        private It should_split_words = () => returnValue.FirstOrDefault().ShouldEqual("al");

        private It should_return_25_words = () => returnValue.Count().ShouldEqual(25);
    }

    public class When_input_string_contains_different_delimeter : word_to_list_provider_concern
    {
        private Establish c =
            () =>
                {
                    text = "al> albums. aver# bar1 barely| be< befoul( bums) by* cat& con^ convex% ely$ foul£ here! hereby¬ jig` jigsaw~ or: saw; tail? tailor/ vex} we{ weaver+";
                };

        private Because b = () => returnValue = sut.ConvertToList(text);

        private It should_split_words = () => returnValue.FirstOrDefault().ShouldEqual("al");

        private It should_return_25_words = () => returnValue.Count().ShouldEqual(25);
    }

    public class When_input_text_is_empty : word_to_list_provider_concern
    {
        private Establish c = () => { emptyString = string.Empty; };

        private Because b = () => results = sut.ConvertToList(emptyString);

        private It should_return_empty_list = () => results.Count().ShouldEqual(0);

        private static string emptyString;
        private static IList<string> results;
    }
}
