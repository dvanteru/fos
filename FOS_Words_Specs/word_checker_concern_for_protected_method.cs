﻿using System.Collections.Generic;
using FOS_Words_Specs.TestingProtectedMethods;
using Machine.Specifications;
using developwithpassion.specifications.rhinomocks;

namespace FOS_Words_Specs 
{
    public abstract class word_checker_concern_for_protected_method : Observes<WordProcessorTest>
    {
        protected static string completeWord;
        protected static IList<string> substringWords;
        protected static string result;
    }

    public class when_asked_to_check_if_a_word_is_formed_of_substrings_in_list : word_checker_concern_for_protected_method
    {
        private Establish c = () =>
            {
                completeWord = "albums";
		substringWords = new List<string>{ "al", "bums"};
            };
        private Because b = () => result = sut.CheckThisWord(completeWord, substringWords);

        private It should_return_word = () => result.ShouldEqual("albums");
    }

    public class when_asked_to_check_word_that_is_not_formed_with_substrings_in_given_list : word_checker_concern_for_protected_method
    {
        private Establish c = () =>
            {
                completeWord = "convex";
                substringWords = new List<string> {"con"};
            };

        private Because b = () => result = sut.CheckThisWord(completeWord, substringWords);

        private It should_return_empty_string = () => result.ShouldBeEmpty();

    }
}
