﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FOS_Words;
using FOS_Words.Provider.contract;
using FOS_Words.Segment;
using FOS_Words.Segment.Contract;
using Machine.Specifications;
using developwithpassion.specifications.extensions;
using developwithpassion.specifications.rhinomocks;

namespace FOS_Words_Specs 
{
    public abstract class words_into_lists_based_on_length_concern : Observes<IWordToListBasedOnLength, WordToListBasedOnLength>
    {
        protected static string text = constants.TEXT_INPUT;

        private Establish c = () =>
            {
                wordtoListProvider = depends.on<IWordToListProvider>();
                returnValue = new List<string>{"al", "albums", "bums", "con", "convex", "vex"};
                wordtoListProvider.setup(x => x.ConvertToList(text)).Return(returnValue);
            };

        protected static IList<String> result;
        protected static IList<String> inputList;
        protected static IWordToListProvider wordtoListProvider;
        protected static List<string> returnValue;
    }

    public class when_asked_to_get_6_letter_words : words_into_lists_based_on_length_concern
    {
        private Because b = () => result = sut.ExtractSixLetterWords(text);

        private It should_ask_word_to_list_provider_to_give_word_list =
            () => wordtoListProvider.received(x => x.ConvertToList(text));

        private It should_extract_list_of_6_letter_words = () => result.Count().ShouldEqual(2);
    }

    public class when_asked_to_extract_less_than_6_letter_words : words_into_lists_based_on_length_concern
    {
        private Because b = () => result = sut.ExtractLessThanSixLetterWords(text);

        private It should_ask_word_to_list_provider_to_give_word_list =
                () => wordtoListProvider.received(x => x.ConvertToList(text));

        private It should_extract_list_of_words_less_than_6_letters = () => result.Count().ShouldEqual(4);
    }
}
