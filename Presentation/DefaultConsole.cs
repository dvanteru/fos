﻿using System;

namespace Presentation
{
    public class DefaultConsole : IDefaultConsole
    {
        public void Write(string line) {
            Console.Write(line);
        }

        public void WriteLine(string line) {
            Console.WriteLine(line);
        }
    }
}