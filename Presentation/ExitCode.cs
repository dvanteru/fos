﻿namespace Presentation
{
    public enum ExitCode 
    {
        InitializationFailure = -1,
        Success = 0
    }
}