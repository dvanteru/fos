﻿namespace Presentation
{
    public interface IDefaultConsole
    {
        void Write(string line);
        void WriteLine(string line);
    }
}