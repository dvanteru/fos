﻿using System;
using FOS_Words.WordVerifier.Contract;

namespace Presentation 
{
    public class Program 
    {
        private readonly IDefaultConsole defaultConsole;
        private readonly IWordProcessor wordProcessor;

        public Program(IDefaultConsole defaultConsole, IWordProcessor wordProcessor)
        {
            this.defaultConsole = defaultConsole;
            this.wordProcessor = wordProcessor;
        }

        static void Main(string[] args) 
        {
            var program = ProgramBuilder.Build(args);
            var exitCode = program == null ? ExitCode.InitializationFailure : program.Run();
            Environment.Exit((int)exitCode);
        }

        private ExitCode Run()
        {            
            var words = this.wordProcessor.WordsFormedOfTwoSubstrings(Constants.InputText);
	    words.ForEach(word => this.defaultConsole.WriteLine(word));
            return ExitCode.Success;
        }
    }
}
