﻿using FOS_Words.Provider;
using FOS_Words.Segment;
using FOS_Words.WordVerifier;

namespace Presentation 
{

    /// <summary>
    /// This can be removed once IOC is implemented.
    /// </summary>
    public class ProgramBuilder 
    {
        public static Program Build(string[] args) {
            var defaultConsole = new DefaultConsole();
            var wordProcessor = new WordProcessor(new WordToListBasedOnLength(new WordToListProvider()));
            return new Program(defaultConsole, wordProcessor);
        }
    }
}
